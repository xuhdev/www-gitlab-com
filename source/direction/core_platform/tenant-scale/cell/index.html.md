---
layout: markdown_page
title: "Category Direction - Cell"
description: ""
canonical_path: "/direction/core_platform/tenant-scale/cell/"
---

- TOC
{:toc}

## Cell

| | |
| --- | --- |
| Stage | [Data Stores](https://about.gitlab.com/direction/core_platform/) |
| Maturity | [Not Applicable](https://about.gitlab.com/direction/#maturity) |
| Content Last Reviewed | `2024-08-20` |

## Introduction and how you can help

Thanks for visiting this category direction page on Cells at GitLab. The Cell category is part of the [Tenant Scale group](https://about.gitlab.com/handbook/product/categories/#tenant-scale-group) within the [Core Platform](https://about.gitlab.com/direction/core_platform/) section and is maintained by [Christina Lohr](https://about.gitlab.com/company/team/#lohrc). 

This vision and direction is constantly evolving and everyone can contribute:
* Please comment in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Atenant%20scale&label_name%5B%5D=Category%3ACell&first_page_size=100) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?state=opened&page=1&sort=start_date_desc&label_name[]=group::tenant+scale&label_name[]=Category:Cell). Sharing your feedback directly on GitLab.com or submitting a merge request to this page are the best ways to contribute to our strategy.
* Please share feedback directly via [email](https://gitlab.com/lohrc), or [schedule a video call](https://calendly.com/christinalohr/30min).
* Please open an issue using the ~"Category:Cell" label, or reach out to us internally in the #g_tenant-scale Slack channel.

## Overview

GitLab.com is currently a single monolithic application that serves all users and organizations. This architecture has several limitations:

- Scalability challenges: As the user base grows, it becomes increasingly difficult to scale the entire application uniformly.
- Performance bottlenecks: Heavy usage by one organization can potentially impact the performance for others.
- Limited isolation: Issues affecting one part of the application can potentially impact all users.
- Maintenance complexity: Updating or maintaining the system requires careful coordination to avoid disrupting all users simultaneously.

The [Organization](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/organization/) and [Cell](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/cells/) concepts aim to address these limitations by introducing a more modular and scalable architecture. Here’s how they work together:

- Organizations provide logical separation: They group related users, groups, and projects under a single entity, allowing for better management and isolation of resources.
- Cells provide physical separation: Each Cell is an independent set of infrastructure components that can host multiple Organizations.

### Goals

By combining Organizations and Cells, we can achieve:

- Improved scalability: We can add new Cells as needed to accommodate growth, rather than scaling the entire application.
- Better performance isolation: Issues or heavy usage in one Cell won’t affect Organizations in other Cells.
- Enhanced reliability: Problems in one Cell are contained, reducing the risk of system-wide outages.
- Easier maintenance: Cells can be updated or maintained independently, minimizing disruption to users.
- Regions: GitLab.com is only hosted within the United States of America. Organizations located in other regions have voiced demand for local SaaS offerings. Cells provide a path towards [GitLab Regions](https://gitlab.com/groups/gitlab-org/-/epics/6037), because Cells may be deployed within different geographies.
- Market segment: Cells provide a solution for organizations in the small to medium business (up to 100 users) and the mid-market segment (up to 2000 users). (See [segmentation definitions](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#segmentation).) Larger organizations may benefit substantially from [GitLab Dedicated](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/index.html).

This approach allows GitLab.com to grow more efficiently while providing a more stable and performant experience for all users.

## Strategy and Themes

We are following an [iterative approach](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/cells/#cells-iterations) to deliver the complete Cells architecture.

## 1 year plan

Over the coming year, we will focus on building the first iteration of Cells: [Cells 1.0](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/cells/iterations/cells-1.0/). The Tenant Scale group will focus on delivering some key components that are required by the project.

### What we are currently working on

- [Cells 1.0 Phase 2: GitLab.com HTTPS Passthrough Proxy](https://gitlab.com/groups/gitlab-org/-/epics/12775)
- [Cells 1.0 Phase 3: GitLab.com HTTPS Session Routing](https://gitlab.com/groups/gitlab-org/-/epics/14509)

### What is planned next

- [Cells 1.0 Phase 4: GitLab.com HTTPS Token Routing](https://gitlab.com/groups/gitlab-org/-/epics/14510)

### What we recently completed

- [Cells 1.0 Phase 1: PreQA Cell](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1293)

### What is Not Planned Right Now

We currently don't plan to implement any scalability solutions for GitLab.com that would negatively impact our self-managed customers. We want all customers to benefit from further scalability.

## Maturity Plan

Cell is a non-marketable category, and is therefore not assigned a maturity level.
